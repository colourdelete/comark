package main

import (
	"gitlab.com/colourdelete/comark/server"
)

func main() {
	server.Run(":8080")
}
